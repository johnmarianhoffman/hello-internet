package main

import (
	pb "gitlab.com/johnmarianhoffman/hello-internet/protocol/generated/hellopb"
	"flag"
	"google.golang.org/grpc"
	log "github.com/sirupsen/logrus"
	"net"
)

func main() {
	
	server_address := "localhost:8080"

	flag.StringVar(&server_address, "a", server_address, "TCP address server should listen on")
	flag.StringVar(&server_address, "address", server_address, "TCP address server should listen on")
	flag.Parse()

	lis, err := net.Listen("tcp", server_address)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption
	
	hs := NewHelloServer()
	grpcServer := grpc.NewServer(opts...)

	pb.RegisterHelloServiceServer(grpcServer, hs)

	log.Infof("gRPC server starting on %v", server_address)

	grpcServer.Serve(lis)

}