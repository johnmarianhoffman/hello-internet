package main

import (
	"context"
	pb "gitlab.com/johnmarianhoffman/hello-internet/protocol/generated/hellopb"
)

type hello struct {
	time string
	ip string
}

type helloServer struct {
	pb.HelloServiceServer
	hellos []hello
}

func NewHelloServer() *helloServer {
	return &helloServer{
		hellos: make([]hello, 0, 10000),
	}
}

func (h *helloServer) Hello(ctx context.Context, req *pb.HelloRequest) (*pb.HelloResponse, error) {
	
	new_hello := hello{
		time: req.GetTime(),
		ip: req.GetIp(),
	}

	h.hellos = append(h.hellos, new_hello)

	return &pb.HelloResponse{}, nil
	
}